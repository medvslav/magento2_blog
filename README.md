# Magento2_Blog
This is module for Magento 2 CMS. This module for adding a blog articles to the Magento stores.

On the frontend, there are pages with a list of articles and pages for one article.
URLs for the frontend list pages - domain_url/blog2.

In the admin panel main menu (sidebar), there is an item - "BLOG2" and submenus - "All Items".
There are standard Magento 2 pages to view, add, edit, delete articles.

The editing article page has fields:

- Blog2 Title

- Author

- Publishing Date
#

- Content

- Image Thumbnail